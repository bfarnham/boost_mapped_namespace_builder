# Boost mapped namespace builder
Builds the boost library from a source zip (i.e. the zip you download from boost.org), BUT!!! maps everything in boost from the default namespace (boost::some_boost_thing_like_thread_for_example) to another
namespace. 

## Background
Boost is an evolving library, functionality is added over time (maybe removed too), therefore from time to time the very latest boost version might have something you'd like to use in 
your application. As far as I'm aware, the 'linux-y' way of bringing boost to a system is to ask the package manager to install it - that's nice; hassle free, probably optimized for the system etc, however,
the package manager (or maybe the repo) decides which boost version will be installed. If it's not the version you need, the version with the latest stuff, then that's a problem.

So you download the latest boost and build it (or not if you can go header only). Great - problem solved. But there's a sneaky problem lurking in this approach - it's a namespace problem; Boost class (etc)
definitions are namespaced by default as boost::whatever, the 'boost' namespace is used regardless of which version of boost the whatever comes from. There's a danger that your application uses some
object (etc) from boost_latest and passes it to some other execution code (in a shared library say) that was linked against boost_system_version. I don't know exactly what would happen in that scenario but
nothing good, that much is certain.

So what do we do? We can take the boost latest download and use a tool (actually a boost tool) called bcp. This tool takes that boost latest code and moves everything from the default boost namespace to another 
(boost version specific) namespace. Also it aliases that version specific namespace to 'boost' so that your application code can just use boost::whatever, but be safe in the knowledge that the 'whatever'
definition is safely resolved to the non default boost version.

Phew: So that's what this python script does - builds boost from a zip file (note *.zip file, not *.tar.gz or other compression formats), and maps the namespace to something other than boost (and then aliases it 
so you can just use boost::whatever in your code).

## Using it
It's a single, simple python script. Just execute the python script and it will tell you what it needs:

python boost_mapped_namespace_builder.py -h

(an example invocation)
python boost_mapped_namespace_builder.py --boost_src_zip $(pwd)/boost_1_59_0.zip --base_dir $(pwd)/work --boost_mapped_namespace_nm boost_1_59_0


