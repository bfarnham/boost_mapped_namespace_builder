import os
import argparse
from argparse import RawTextHelpFormatter
from subprocess import CalledProcessError
import zipfile
import sys
import subprocess
import shutil
import glob
import time
import platform

windows_tools = {'UNZIP':'"C:/Program Files/7-Zip/7z.exe"', 'UNZIP_ARGS': 'x {0} -o{1}' , 'BOOTSTRAP': 'bootstrap' , 'B2':'b2' , 'BCP':'bcp' }
linux_tools = {'UNZIP': 'unzip', 'UNZIP_ARGS': '-u {0} -d {1}' , 'BOOTSTRAP': 'sh bootstrap.sh', 'B2':'./b2' , 'BCP':'./bcp' }

working_dirs = {'DEFAULT_NAMESPACE':"", 'DEFAULT_NAMESPACE_INSTALL':"", 'MAPPED_NAMESPACE':"", 'MAPPED_NAMESPACE_INSTALL':""}
boost_libs = 'algorithmm asio atomic bimap bind chrono circular_buffer config container context convert core coroutine coroutine2 date_time endian filesystem foreach format graph interprocess io iostreams iterator lambda lexical_cast locale lockfree log math move numeric parameter predef program_options python random range ratio regex serialization signals signals2 smart_ptr system test thread timer tools tuple utility variant wave'

def run_command(cmd_purpose_string, cmd, args, working_dir=""):
	print(f'executing cmd for purpose [{cmd_purpose_string}], cmd [{cmd}], args [{args}], working_dir [{working_dir}]')

	if working_dir:
		try:
			os.chdir(working_dir)
		except OSError as e:
			sys.exit(f'chdir failed set cwd to [{working_dir}], error message [{e.message}]')

	try:
		subprocess.check_call(cmd+" "+args, shell=True)
	except CalledProcessError as e:
		sys.exit(f'cmd exec for [{cmd_purpose_string}] failed, cmd [{e.cmd}], return code [{e.returncode}]')
	except Exception as e:
		sys.exit(f'cmd exec for [{cmd_purpose_string}] failed, unknown error. Exception message: {e.message}')

def get_toolset():
	plat = platform.system().lower()
	if plat == 'windows':
		return 'msvc-14.3' # visual studio 2017 (msvc version -> human names mapping available on wikipedia. Ask uncle google).
	elif plat == 'linux':
		return 'gcc'
	else:
		sys.exit(f'unrecognized platform [{plat}], no toolset defined.')

def get_platform_specific_cmds():
	plat = platform.system().lower()
	if plat == 'windows':		
		return windows_tools
	elif plat == 'linux':
		return linux_tools
	else:
		sys.exit(f'unrecognized platform [{plat}], no toolset defined.')

def get_platform_specific_cmd_line(cmd_key):
	platform_specific_dictionary = get_platform_specific_cmds()
	if cmd_key in platform_specific_dictionary:
		return platform_specific_dictionary[cmd_key]
	else:
		sys.exit(f'unrecognized command key [{cmd_key}] for plafform [{plat}], exiting')
	 

def process_args():
	print('===processing command line args==')
	parser = argparse.ArgumentParser(description="""The boost mapped namespace builder. Takes zipped boost source code (from boost.org), maps the namespace (e.g. boost::shared_ptr to boost_1_59_0::shared_ptr) then builds the boost library.
	Directory structure:
	BASE-DIR (e.g. C:\\3rdPartySoftware\\boost\\1.59.0)
	--DEFAULT_NAMESPACE (boost source unzipped to here: "pristine" code)
	--DEFAULT_NAMESPACE_INSTALL (minmimal build, basically just boost bcp tool - handles namespace mapping)
	--MAPPED_NAMESPACE (boost source code files with mapped namespace e.g. boost => boost_1_59_0)
	--MAPPED_NAMESPACE_INSTALL (install dir: headers and compiled libs. The holy grail)""", formatter_class=RawTextHelpFormatter)
	parser.add_argument('--boost_src_zip', required=True, help='absolute path to local boost source code archive, downloaded from www.boost.org, run with -h for details')
	parser.add_argument('--base_dir', required=True, help='absolute path to directory in which boost will be unzipped, mapped and built, run with -h for details')
	parser.add_argument('--boost_mapped_namespace_nm', required=True, help='the new boost namespace, e.g. boost_1_59_0, run with -h for details')
	args = parser.parse_args()
	print(f'using boost zip [{args.boost_src_zip}]')
	print(f'using BASE-DIR [{args.base_dir}]')
	print(f'mapping boost:: namespace to [{args.boost_mapped_namespace_nm}::]')
	print(f'building with toolset [{get_toolset()}], platform [{platform.system()}]')
	return args

def setup_dirs(base_dir):
	print('==setting up dir paths==')
	for curr_dir in working_dirs:
		working_dirs[curr_dir] = os.path.join(base_dir, curr_dir)

def create_dirs(base_dir):
	print('==creating working directories==')
	for curr_dir in working_dirs:
		try:
			if os.path.exists(working_dirs[curr_dir]):
				print(f"deleting existing dir [{working_dirs[curr_dir]}]")
				shutil.rmtree(working_dirs[curr_dir], ignore_errors=True)
			os.mkdir(working_dirs[curr_dir])
		except Exception as e:
			sys.exit(f'unexpected error whilst dealing with [{working_dirs[curr_dir]}], error msg: {e.message}')

def unzip_boost(boost_src_zip):	
	print(f"==extracting boost zip file [{boost_src_zip}], to [{working_dirs['DEFAULT_NAMESPACE']}] may take a while...==")
	if not zipfile.is_zipfile(boost_src_zip):
		sys.exit(f"python does not recognize file [{boost_src_zip}] as a zip file")

	cmd = get_platform_specific_cmd_line('UNZIP')
	args = get_platform_specific_cmd_line('UNZIP_ARGS').format(boost_src_zip, working_dirs['DEFAULT_NAMESPACE'])
	run_command('unzip boost', cmd, args)

	dir_contents = os.listdir(working_dirs['DEFAULT_NAMESPACE'])
	if len(dir_contents) != 1:
		sys.exit(f"expected just one directory (but found count [{len(dir_contents)}] - contents [{dir_contents}]), the pristine unzipped boost directory (e.g. boost_1_59_0) in [{working_dirs['DEFAULT_NAMESPACE']}]")
	return os.path.join(working_dirs['DEFAULT_NAMESPACE'], dir_contents[0])

def bootstrap_builder_for_default_namespace(boost_unzip_dir):
	print('==building boost builder bootstrapper, default namespace==')
	working_dir = boost_unzip_dir
	cmd = get_platform_specific_cmd_line('BOOTSTRAP')
	args = '--prefix={0}'.format(working_dirs['DEFAULT_NAMESPACE_INSTALL'])
	run_command('build default namespace boost bootstrapper', cmd, args,  working_dir)

def build_bcp_tool(boost_unzip_dir):	
	print('==building bcp tool==')
	working_dir = boost_unzip_dir
	cmd = get_platform_specific_cmd_line('B2')
	args = f'tools/bcp toolset={get_toolset()}'
	run_command('build boost bcp tool', cmd, args, working_dir)

def use_bcp_to_create_mapped_namespace_source(boost_unzip_dir, boost_target_namespace):
	print('==creating mapped namespace boost source code using bcp==')
	orig_src_dir = boost_unzip_dir
	mapped_src_dir = working_dirs['MAPPED_NAMESPACE']
	
	working_dir = os.path.join(boost_unzip_dir, 'dist', 'bin')
	cmd = get_platform_specific_cmd_line('BCP')
	args = '--boost={0} --namespace={1} --namespace-alias {2} {3}'.format(orig_src_dir, boost_target_namespace, boost_libs, mapped_src_dir)
	run_command('map boost lib to new namespace', cmd, args, working_dir)

def bootstrap_builder_for_mapped_namespace(boost_unzip_dir):
	print('==building boost builder bootstrapper, mapped namespace==')

	jam_files = glob.glob(os.path.join(boost_unzip_dir, '*.jam'))
	bootstrap_files = glob.glob(os.path.join(boost_unzip_dir, 'bootstrap*'))
	files_to_copy = jam_files + bootstrap_files
	for bootstrap_file in files_to_copy:
		shutil.copy(bootstrap_file, working_dirs['MAPPED_NAMESPACE'])	

	working_dir = working_dirs['MAPPED_NAMESPACE']
	cmd = get_platform_specific_cmd_line('BOOTSTRAP')
	args = '--prefix={0}'.format(working_dirs['MAPPED_NAMESPACE_INSTALL'])
	run_command('build mapped namespace boost bootstrapper', cmd, args,  working_dir)

def build_mapped_namespace_src():
	print('==building the mapped boost namespace source code==')
	tmp_build_dir = os.path.join(working_dirs['MAPPED_NAMESPACE'], 'build-dir')
	install_dir = working_dirs['MAPPED_NAMESPACE_INSTALL']
	working_dir = working_dirs['MAPPED_NAMESPACE']
	cmd = get_platform_specific_cmd_line('B2')

	args = f'install -a --build-dir={tmp_build_dir} --prefix={install_dir} cxxflags=-fPIC address-model=64 toolset={get_toolset()} --build-type=minimal --layout=versioned '
	run_command('build mapped namespace source code', cmd, args, working_dir)

# Sample invocations - note both invocation examples below are run from directories containing this python script
# and the boost source zip.

# Sample invocation - on windows (from a VCVARSALL.bat window)
# C:\Workspace\boost_mapped_namespace_builder>python boost_mapped_namespace_builder.py --boost_src_zip %cd%\boost_1_59_0.zip --base_dir %cd% --boost_mapped_namespace_nm boost_1_59_0

# Sample invocation - on linux
# python boost_mapped_namespace_builder.py --boost_src_zip $(pwd)/boost_1_59_0.zip --base_dir $(pwd) --boost_mapped_namespace_nm boost_1_59_0

start_time = time.time()

args = process_args()
setup_dirs(args.base_dir)
create_dirs(args.base_dir)
boost_unzip_dir = unzip_boost(args.boost_src_zip)
bootstrap_builder_for_default_namespace(boost_unzip_dir)
build_bcp_tool(boost_unzip_dir)
use_bcp_to_create_mapped_namespace_source(boost_unzip_dir, args.boost_mapped_namespace_nm)
bootstrap_builder_for_mapped_namespace(boost_unzip_dir)
build_mapped_namespace_src()

print(f'build completed in [{time.time()-start_time}] seconds')
